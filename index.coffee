fs = require 'fs'
doctype 5
html ->
	head ->
		meta charset: 'utf-8'
		meta name: 'viewport', content: 'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1'
		title 'Loading...'
		link rel: 'shortcut icon'
		style STYLE
		try for polyfill in fs.readdirSync './polyfills' then script src: "polyfills/#{polyfill}"
		for     lib      in fs.readdirSync './libs'      then script src: "libs/#{lib}"
		coffeescript SCRIPT
	body ->
		div id: 'loading-file', class: 'center', ->
			svg class: 'center', viewBox: '0 0 32 32', -> path fill: 'rgba(0,0,0,0.2)', stroke: 'rgba(0,0,0,0.8)', 'stroke-width': '0.1px', d: 'M16 0 A16 16 0 0 0 16 32 A16 16 0 0 0 16 0 M16 4 A12 12 0 0 1 16 28 A12 12 0 0 1 16 4'
			svg class: 'center spin', viewBox: '0 0 32 32', -> path fill: 'white', d: 'M16 0 A16 16 0 0 1 32 16 L28 16 A12 12 0 0 0 16 4z'
			div id: 'loading-file-percent', class: 'center', -> 'Connecting...'
		div id: 'loading-results', class: 'hide', style: 'width: 0%'
		div id: 'messages', class: 'frame fade-in-out hide'
		div id: 'settings', class: 'frame hide', ->
			button id: 'close_settings', title: 'Close settings', 'Settings'
			h1 'Interface'
			p ->
				label for: 'interface_side', 'Display sidebar on the '
				select id: 'interface_side', ->
					option value: 'left',  'left'
					option value: 'right', 'right'
			h1 'Sites'
			div id: 'third_party_site_disclaimer', class: 'fade-out', ->
				h2 'Third-party site disclaimer'
				p class: 'text', ->
					text """The default image board in #{NAME} is curated to only contain images that follow """
					a href: 'https://play.google.com/about/developer-content-policy.html', target: '_blank', "Google Play's Developer Content Policy"
					text """. #{NAME} (like most web browsing applications) allows the user to enter the addresses of third-party websites outside of the application (or it's developers) control. #{NAME} contains a rating filter, but this relies upon how the websites themselves rate their content. Due to this the developers of #{NAME} can not be held liable if content retrieved from third-party websites is offensive or illegal in your territory."""
				button id: 'third_party_site_disclaimer_dismiss', 'I understand, senpai.'
			div id: 'sites', ->
				div id: 'new_site', ->
					input id: 'new_site_address', type: 'url', placeholder: 'Add new site: example.com'
		div id: 'sidebar', class: 'frame hide', ->
			a id: 'info-version', href: 'http://zenbooru.org', target: '_blank', "#{NAME} #{VERSION}"
			button id: 'open_settings', title: 'Open settings', 'Settings'
			br()
			br() # Required for layout, see commit #55a1cbc
			input id: 'search', type: 'search', placeholder: 'Search: tag_a tag_b tag_c'
			p ->
				label for: 'highest_rating', 'Highest Rating: '
				select id: 'highest_rating', ->
					option value: 'explicit',             'Explicit'
					option value: 'questionable',         'Questionable'
					option value: 'safe', selected: true, 'Safe'
			table id: 'excluded_tags', class: 'fade-in-out hide close', ->
				thead ->
					th id: 'excluded_tags-name', colspan: 3, 'Excluded Tags'
					th id: 'excluded_tags-total',            'Total'
				tbody class: 'fade-in-out'
			p id: 'results-counter', class: 'fade-in-out hide close', ->
				span id: 'current_index', '0'
				text ' of '
				span id: 'last_index', '0'
				br()
				button id: 'go_to_first', title: 'Return to the first result in the current search',     '|< Return to start'
				br()
				button id: 'go_previous', title: 'Go back to the previous result in the current search', '<< Backwards'
				button id: 'go_next',     title: 'Go to the next result in the current search',          'Forwards >>'
			p -> a id: 'save_file', class: 'fade-in-out hide close', href: '', title: 'Open a file dialog to save the current file permantly', download: 'true', target: '_blank', 'Save as...'
			table id: 'display-options', class: 'fade-in-out hide close', ->
				thead ->
					th colspan: 2, 'Display options'
				tbody ->
					tr ->
						th -> label for: 'display-file', 'Viewing: '
						td ->
							select id: 'display-file', ->
								option value: 'original', selected: true, 'Original'
								option value: 'sample',                   'Sample'
								option value: 'video',                    'Video'
							button id: 'display-file-default', class: 'fade-in-out hide close', 'Set as default'
					tr ->
						th -> label for: 'display-background', 'Background: '
						td -> select id: 'display-background', ->
							option value: 'transparent', selected: true, 'Transparent'
							option value: 'white',                       'White'
							option value: 'black',                       'Black'
					tr ->
						td id: 'display-filters', colspan: 2, ->
							button
								id: 'display-scaling',
								'data-title-false': "Use nearest neighbour scaling to preserve crisp edges (ideal for pixel art).",
								'data-text-false':  "Sharp scaling",
								'data-title-true':  "Use smooth scaling to keep images looking smooth and realistic (ideal for paintings or photos).",
								'data-text-true':   "Smooth scaling"
							button
								id: 'display-invert',
								'data-title-false': "Invert the current image's colours.",
								'data-text-false':  "Invert"
								'data-title-true':  "Return the current image to its original colours",
								'data-text-true':   "Uninvert"
							button
								id: 'display-notes',
								disabled: true,
								'data-title-false': "Hide any visible notes.",
								'data-text-false':  "Hide notes",
								'data-title-true':  "Show any visible notes.",
								'data-text-true':   "Show notes"
			table id: 'tags', class: 'fade-in-out hide close', ->
				thead ->
					th id: 'tags-name', colspan: 3, 'Tags'
					th id: 'tags-total',            'Total'
				tbody()
			table id: 'results-site', class: 'fade-in-out hide close', ->
				thead -> th 'Sites'
				tbody()
			table id: 'results-source', class: 'fade-in-out hide close', ->
				thead -> th 'Source'
				tbody()
			table id: 'results-uploaded', class: 'fade-in-out hide close', ->
				thead -> th 'Uploaded'
				tbody()
			table id: 'results-notes', class: 'fade-in-out hide close', ->
				thead -> th colspan: 2, 'Notes'
				tbody()
		div id: 'gestures'
		div class: 'view'
		div id: 'bottombar', class: 'frame bottom hide', -> div id: 'thumbs'
