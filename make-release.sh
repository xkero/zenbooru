#!/usr/bin/env bash

set -e # exit on any errors

tag="$1"
project="${2:-'Untitled *booru viewer'}"
directory=$(echo "$project-$tag" | sed 's/ /-/g;s/[^a-zA-Z0-9_.-]//g;s/.*/\L&/') # keep the filename cross platform friendly
build="/tmp/build/$directory"
package="$PWD/$directory.zip"
sources="make.coffee index.coffee src/ theme/ platforms/portable/"

rm -r "$build"   2>/dev/null ||:
rm    "$package" 2>/dev/null ||:

mkdir -p "$build"
git archive "$tag" | tar -C "$build" -x $sources libs/
ln -s "$PWD/node_modules" "$build/node_modules"
cd "$build"
rm platforms/portable/libs
mv platforms/portable/* .
coffee ./make.coffee "$tag" "$project"
mv index.html Loading
rm -r $sources platforms/
rm node_modules
cd ..
zip -r "$package" "$directory"
echo "Built release package: $package"
