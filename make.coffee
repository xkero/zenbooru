#!/usr/bin/env coffee
fs   = require 'fs'
styl = require 'stylus'
exec = require('child_process').execFileSync
open = (file) -> fs.readFileSync(file).toString()

style = styl(open './theme/index.styl').set('paths', ['./theme/']).render (error, style) ->
	if error?
		console.log error
		return
	name    = process.argv[3] or 'Untitled *booru viewer'
	html    = open('index.coffee').replace /\n/g, '\n\t'
	script  = (open "./src/#{file}" for file in fs.readdirSync('./src') when file[0] isnt '.').join('\n').replace /\n/g, '\n\t'
	version = process.argv[2] or exec('git', ['describe', '--abbrev=0', '--tags']).toString().replace /\n/g, ''
	# Gets function names from coffeescript files and hopfully nothing else, won't handle multiline strings properly though
	magic = /(?:(?:^|->|then)\s*)((?!if|else|try|catch|for|while|switch|when|not|is|isnt|\w+\s*[-+\*\/%<>&^|]*[=:]|\w+\.)\w+)/gm
	tags = []
	while ((tag = magic.exec(html)) isnt null)
		if (tag.index is magic.lastIndex) then magic.lastIndex++
		if tags.indexOf(tag[1]) is -1 then tags.push tag[1]
	
	template = """
		#{("#{tag} = (attr...) -> tag.apply @, ['#{tag}'].concat attr" for tag in tags).join '\n'}
		{render, tag, doctype, style, script, coffeescript, br, text} = require 'teacup'
		NAME = '#{name}'
		VERSION = '#{version}'
		SCRIPT = ->
			#{script}
		#SCRIPT = 'var __slice = [].slice, '+SCRIPT.toString().slice(22,-2)
		STYLE = '''
			#{style}
		'''
		output = render ->
			#{html}
		console.log output
	"""
	fs.writeFileSync 'index.html', exec('coffee', ['-s'], input: template).toString()
	console.log 'Built successfully'
