#!/usr/bin/env bash

site="$1"
tags="${@:2}"
results="results/$tags-$(echo $1 | sed 's!^http://\|/$!!g').xml"
url="$site/index.php?page=dapi&s=post&q=index&tags=$tags"
mkdir -p results &>/dev/null
mkdir -p images  &>/dev/null

echo "Downloading first page for $tags from $site..."
wget "$url&pid=0" -O "$results"

images=$(awk '/count/{print$2;exit}' RS=\  FS=\" "$results")
pages=$(($images/100))

[[ $pages > 0 ]] && (
	echo "Downloading remaining pages ($pages)..."
	wget $(eval 'echo "'$url'&pid="{1..'$pages'}') -O - >> "$results" # yeah...
)

echo "Downloading images..."
awk '/file_url/{print$2}' FS=\" RS=\  "$results" | xargs wget -P images -nc
