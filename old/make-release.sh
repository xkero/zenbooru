#!/usr/bin/env bash

tag="$@"
project="untitled-booru-viewer"
build="/tmp/build/$project-$tag"
package="$PWD/$project-$tag.zip"

rm -r "$build"   2>/dev/null
rm    "$package" 2>/dev/null

mkdir -p "$build" &&\
git archive "$tag" | tar -C "$build" -x view.coffee start.anyOS.bat &&\
cd "$build" &&\
coffeecup view.coffee &&\
rm view.coffee &&\
cd .. &&\
zip -r "$package" "$project-$tag" &&\
( echo "Built release package: $package" ; exit 0) ||\
( error=$? ; echo "Something went wrong :(" ; exit $error )