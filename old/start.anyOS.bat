#!/bin/sh
: # This is a sh/batch hybrid script that should work on Linux, OSX or Windows.
: # This application requires Google Chrome (or Chromium) to be installed to run.
: # If this fails to work on your system please let me know at <sinister.ray@gmail.com>
:; alias browser=$(for exec in 'google-chrome' 'chrome' 'chrome-browser' 'chromium' 'chromium-browser'; do which "$exec" 2>/dev/null && break; done || echo 'open -a "Google Chrome" -n --args') #
:; browser --enable-deferred-image-decoding --disable-web-security --user-data-dir="$PWD/chrome_data" --app="file://$PWD/view.html" ; exit $? #
@ECHO OFF
start chrome --enable-deferred-image-decoding --disable-web-security --user-data-dir="%cd%/chrome_data" --app="file://%cd%/view.html"
