doctype 5
html ->
	head ->
		stylus '''
			*
				-webkit-backface-visibility hidden // Fixes flicker bug in webkit/chrome by making it use a different transform method without the bug
			html, body
				color white
				background black
				font-family sans-serif
				margin 0
				text-align center
				-webkit-user-select none
				user-select none
				overflow hidden
				height 100%
				width  100%
				text-shadow:
					 1px  1px 1px black,
					-1px  1px 1px black,
					 1px -1px 1px black,
					-1px -1px 1px black,
					 0    0   1em black,
					 0    0   1em black
			#loading
				zoom 5
				-webkit-filter drop-shadow(0 0 0.2em black)
				   -moz-filter drop-shadow(0 0 0.2em black)
				        filter drop-shadow(0 0 0.2em black)
			.center
				position fixed
				top    0
				bottom 0
				left   0
				right  0
				margin auto
			#next img.center.natural
				position absolute
			.frame
				background-color black
				background-image:
					linear-gradient( 45deg, #111 25%, transparent 25%, transparent 75%, #111 75%, #111),
					linear-gradient(-45deg, #111 25%, transparent 25%, transparent 75%, #111 75%, #111)
				background-size 3em 3em
				//background-position 0 0, 2.5em 2.5em
				position absolute
				top  0
				left 0
				height 100%
				width  100%
				transition opacity 0.3s ease-in-out
				img
					box-shadow 0 0 1em black
			img.tall
				height 100%
				width  auto
			img.wide
				height auto
				width  100%
			img.pixel_art
				image-rendering pixelated
			.hide
				opacity 0
				transition none // Clear transition to instantly hide, otherwise we'll fade-out and overlap with our fade-in
			#loading.hide
				display none
			#sidebar
				//background-image linear-gradient(-90deg, hsla(0,0,0,1) 1em, hsla(0,0,0,0.2) 100%)
				max-height 100%
				width auto
				overflow-x hidden
				overflow-y auto
				position absolute
				right -10em
				top     0em
				padding 0.5em
				text-align right
				opacity 0
				transition all 0.3s ease-in-out
				box-shadow 0 0 1em black
				border-bottom-left-radius 1em
				//-webkit-mask-image linear-gradient(to top, transparent 0, black 2em)
				//         -moz-mask linear-gradient(to top, transparent 0, black 2em)
				&:hover //, &:before:hover
					opacity 1
					right 0em
				//*
				&:before
					position absolute
					top   0
					right 0
					display block
					border-bottom-left-radius 1em
					content ""
					width 100%
					height 1000%
					overflow hidden
					dark  = hsl(0, 0%, 0%)
					light = hsl(0, 0%, 5%)
					background repeating-linear-gradient(-45deg, light, light 25%, dark 25%, dark 50%, light 50%) top left fixed
					background-size 30px 30px
					transition all 0.3s ease-in-out
					-webkit-mask-composite destination-over
					-webkit-mask-image linear-gradient(-90deg, black  0%, transparent 99%), linear-gradient(to top, transparent 0, black 2em)
					         -moz-mask linear-gradient(-90deg, black  0%, transparent 99%)
				*
					position relative
					z-index 1
				//*/
			#tags
				list-style-type none
			:link, :visited
				color white
				text-decoration none
				text-shadow:
					 1px  1px 1px black,
					-1px  1px 1px black,
					 1px -1px 1px black,
					-1px -1px 1px black,
					 0    0   1em black,
					 0    0   1em black
				transition all .7s ease-out
			:link:hover, :visited:hover
				color hsl(85, 100%, 60%)
				transition all .1s ease-in
				text-shadow:
					 1px  1px 1px hsl(85, 100%, 20%),
					-1px  1px 1px hsl(85, 100%, 20%),
					 1px -1px 1px hsl(85, 100%, 20%),
					-1px -1px 1px hsl(85, 100%, 20%),
					 0    0   1em hsl(85, 100%, 20%),
					 0    0   1em hsl(85, 100%, 20%)
		'''
		coffeescript ->
			RIGHT     =  39
			PERIOD    = 190
			LEFT      =  37
			COMMA     = 188
			after     = (ms, callback)  -> setTimeout callback, ms*1000
			commas    = (num)           -> String(num).replace /\B(?=(\d{3})+(?!\d))/g, ","
			getType   = (object)        -> Object::toString.call object
			make      = (tag, props...) ->
				element = document.createElement tag
				for prop in props then switch
						when getType(prop) is '[object String]' then element.innerHTML = prop
						when prop instanceof HTMLElement then element.appendChild prop
						else for key, value of prop then switch key
							when 'events' then element.addEventListener(key, value) for key, value of value
							else element.setAttribute(key, value)
				element
			S = (query) ->
				results = document.querySelectorAll query
				if results.length is 1 then results[0] else results
			window.index     =  0
			window.loadTimer =  0
			window.imageZoom =  0
			window.allImages = {}
			window.lastTerms = {}
			window.addEventListener 'load', ->
				window.allImages = images
				
				window.addEventListener         'resize', aspect
				S('#next img').addEventListener 'load',   loaded
				#S('#next img').addEventListener 'error',               -> if @classList.contains('forward') > -1 then move 1 else move -1
				S('#next').addEventListener     'click',       (event) -> switch event.button
					when 0 then move 1
					when 1
						if S('#next img').classList.contains 'natural'
							S('#next img').classList.remove 'natural'
							aspect()
						else
							S('#next img').classList.remove 'tall', 'wide'
							S('#next img').classList.add    'natural'
						aspect()
				S('#next').addEventListener     'contextmenu', (event) -> move -1 ; event.preventDefault()
				S('#sidebar').addEventListener  'mouseleave',          -> S('#search').blur()
				document.addEventListener       'keyup',       (event) ->
					if document.querySelectorAll('input:focus').length > 0 then return
					amount = if event.shiftKey then 10 else 1
					# TODO: Put in check for input fields or anywhere else we don't want to steal keyboard input from the web browser
					switch event.keyCode
						when RIGHT, PERIOD then move amount
						when LEFT,  COMMA  then move amount * -1
						else return
					event.preventDefault()
				
				S('#view_all').innerHTML = "All images (#{commas(allImages.length)})"
				S('#view_all').addEventListener 'click', (event) -> search() ; event.preventDefault()
				S('#search').addEventListener   'change',        -> search @value.split /\s+/
				move 0
			
			updateTitle = -> document.title = "#{commas(window.index+1)}/#{commas(images.length)}: #{images[window.index]} [#{window.imageZoom}%]"
			
			move = (amount) ->
				window.index += amount
				# check if out of range
				if     (window.index < 0)               then window.index += images.length # go to end
				else if(window.index > images.length-1) then window.index -= images.length # go to start
				S('#prev img').src = S('#next img').src
				S('#prev img').setAttribute 'class', S('#next img').getAttribute 'class'
				S('#next').classList.remove 'forward', 'backward'
				S('#next').classList.add 'hide', if amount > 0 then 'forward' else 'backward'
				S('#next img').src = "images/#{images[window.index]}"
				window.loadTimer = after 0.15, -> S('#loading').classList.remove 'hide'
			
			aspect = ->
				window.imageZoom = Math.floor S('#next img').height / S('#next img').naturalHeight * 100
				updateTitle()
				windowAspect = window.innerHeight / window.innerWidth
				for image in [S('#next img'), S('#prev img')] when image.classList.contains('natural') is false
					imageAspect = image.naturalHeight / image.naturalWidth
					image.classList.remove 'tall', 'wide'
					image.classList.add if imageAspect > windowAspect then 'tall' else 'wide'
			
			loaded = ->
				aspect()
				S('#next img').classList.remove 'pixel_art'
				tagLinks = document.createDocumentFragment()
				tags[images[window.index]].forEach (tag) ->
					tagLinks.appendChild make 'li', make 'a', class: 'tag', href: '', "#{tag} (#{commas totals[tag]})", events: click: (event) -> search([tag]) ; event.preventDefault()
					# While oekaki isn't strictly always done in pixel art style, most of it looks better with nearest neighbour interpolation
					if(tag in ['pixel_art', 'oekaki']) then S('#next img').classList.add 'pixel_art'
				S('#tags').innerHTML = ''
				S('#tags').appendChild tagLinks
				
				clearTimeout window.loadTimer
				S('#next').classList.remove 'hide'
				S('#loading').classList.add 'hide'
			
			search = (terms=[]) ->
				terms.push 'rating:safe'
				if terms? and terms.length > 0
					window.lastTerms = terms
					results = []
					for image in window.allImages
						broke = false
						for term in terms
							if tags[image].indexOf(term) is -1
								broke = true
								break
						results.push(image) unless broke
					window.images = results.sort()
				else window.images = window.allImages
				move window.index = 0
				loaded() # Calling move(0) sets the same image we already loaded so the onload event doesn't fire and we need to call loaded ourselves
			window.search = search
			window.move   = move
		script src: 'all.js' # Insert jsonp file here
	body ->
		div id: 'prev', class: 'frame', ->
			img class: 'center'
			svg id: 'loading', class: 'center', xmlns: 'http://www.w3.org/2000/svg', width: '32', height: '32', fill: 'white', ->
				path opacity: '.25', d: 'M16 0 A16 16 0 0 0 16 32 A16 16 0 0 0 16 0 M16 4 A12 12 0 0 1 16 28 A12 12 0 0 1 16 4', fill: 'black'
				path d: 'M16 0 A16 16 0 0 1 32 16 L28 16 A12 12 0 0 0 16 4z', ->
					animateTransform attributeName: 'transform', type: 'rotate', from: '0 16 16', to: '360 16 16', dur: '0.8s', repeatCount: 'indefinite'
		div id: 'next', class: 'frame', ->
			img class: 'center'
		div id: 'sidebar', ->
			label for: 'search', -> 'Search: '
			input id: 'search', type: 'search', placeholder: 'tag_a tag_b tag_c'
			p -> a id: 'view_all', href: ''
			ul id: 'tags'
		div id: 'thumbnails', ->
			img()
