#!/usr/bin/env coffeecup
version = 'v0.2.0'
doctype 5
html ->
	head ->
		meta charset: 'utf-8'
		title: 'Loading...'
		link rel: 'shortcut icon'
		stylus '''
			*
				-webkit-backface-visibility hidden // Fixes flicker bug in webkit/chrome by making it use a different transform method without the bug
			html, body
				color white
				background black
				font-family sans-serif
				margin 0
				text-align center
				-webkit-user-select none
				user-select none
				overflow hidden
				height 100%
				width  100%
			text-outline(color)
				text-shadow 1px 1px 1px color, -1px  1px 1px color, 1px -1px 1px color, -1px -1px 1px color
			text-outline-shadow(color)
				text-shadow 1px 1px 1px color, -1px  1px 1px color, 1px -1px 1px color, -1px -1px 1px color, 0 0 0.5em color, 0 0 0.5em color
			body
				text-outline-shadow black
			#loading
				-webkit-filter drop-shadow(0 0 0.2em black)
				   -moz-filter drop-shadow(0 0 0.2em black)
				        filter drop-shadow(0 0 0.2em black)
			.center
				position fixed
				top    0
				bottom 0
				left   0
				right  0
				margin auto
			#next img.center.natural
				position absolute
			.frame
				background-color black
				background-image:
					linear-gradient( 45deg, #111 25%, transparent 25%, transparent 75%, #111 75%, #111),
					linear-gradient(-45deg, #111 25%, transparent 25%, transparent 75%, #111 75%, #111)
				background-size 3em 3em
				position absolute
				top  0
				left 0
				height 100%
				width  100%
				img
					box-shadow 0 0 1em black
			img.tall
				height 100%
				width  auto
			img.wide
				height auto
				width  100%
			img.pixel_art
				image-rendering pixelated
			.fade-in-out, .fade-in, .fade-out.hide
				transition opacity 0.3s ease-in-out
			.fade-in.hide, .fade-out
				transition none // Clear transition to instantly hide, otherwise we'll fade-out and overlap with our fade-in
			.hide
				opacity 0
			.close
				display none
			.right
				text-align right
				right 0em
				direction ltr
				& *
					direction initial
				& #open_settings:after
					content ' ☰'
				& #close_settings:after
					content ' ✕'
				& #info-version
					float left
			.left
				text-align left
				left 0em
				direction rtl
				& *
					direction initial
				& #open_settings:before
					content '☰ '
				& #close_settings:before
					content '✕ '
				& #info-version
					float right
			#info-version
				margin-top 0.5em
			h1, th
				text-align center
			#settings, #sidebar
				z-index 1
				top 0em
				max-height 100%
				width auto
				overflow-x hidden
				overflow-y auto
				position absolute
				padding 0.5em
				transition all 0.3s ease-in-out
				&.right
					right 0em
					&.hide
						right -10em
				&.left
					left  0em
					&.hide
						left  -10em
				&.hide
					z-index 0
					& *
						pointer-events none
			#tags
				list-style-type none
				white-space nowrap
			:disabled
				cursor not-allowed
				color gray
			:link, :visited, input, button, select
				color white
				text-decoration none
				transition all .7s ease-out
				text-outline black
			:link, :visited, button
				text-outline-shadow black
			:link:hover, :visited:hover, button:hover:not(:disabled), select:hover:not(:disabled), input[type="checkbox"]:hover:after
				color hsl(85, 100%, 60%)
				transition all 0.1s ease-in
				text-outline-shadow hsl(85, 100%, 20%)
			inputs   = 'select, input[type="text"], input[type="search"], input[type="password"], input[type="url"], input[type="checkbox"]'
			controls = 'select, button, input[type="checkbox"]'
			{inputs}, {controls}
				-webkit-appearance none
				        appearance none
				font-size 100%
				padding 0.3em
				border none
			{inputs}
				background rgba(0,0,0,0.6)
				box-shadow inset 0 0 1em rgba(0,0,0,0.2)
				border-radius 0.2em
			{controls}
				cursor pointer
			button
				background none
			select
				background-image url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='32px' width='32px'><text x='16' y='20' fill='white' font-size='20' style='text-shadow: 1px  1px 1px black, -1px  1px 1px black, 1px -1px 1px black, -1px -1px 1px black'>▾</text></svg>")
				background-repeat no-repeat
				background-position top right
				padding-right 1em
			select:hover:not(:disabled)
				background-image url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='32px' width='32px'><text x='16' y='20' fill='hsl(85, 100%, 60%)' font-size='20' style='text-shadow: 1px  1px 1px hsl(85, 100%, 20%), -1px  1px 1px hsl(85, 100%, 20%), 1px -1px 1px hsl(85, 100%, 20%), -1px -1px 1px hsl(85, 100%, 20%), 0    0   0.5em hsl(85, 100%, 20%), 0    0   0.5em hsl(85, 100%, 20%)'>▾</text></svg>")
			input[type="checkbox"]
				width  1.75em
				height 1.75em
				color white
				padding 0
				&:after
					transition all .1s ease-in
					content '✓'
					opacity 0
					font-size 200%
					position relative
					top -0.1em
					left 0.05em
					overflow hidden
				&:checked:after
					opacity 1
			:focus
				outline none
			#new_site_add
				width 100%
		'''
		coffeescript ->
			RIGHT     =  39
			PERIOD    = 190
			LEFT      =  37
			COMMA     = 188
			
			after     = (ms, callback)  -> setTimeout callback, ms*1000
			commas    = (num)           -> String(num).replace /\B(?=(\d{3})+(?!\d))/g, ","
			getType   = (object)        -> Object::toString.call object
			make      = (tag, props...) ->
				element = document.createElement tag
				for prop in props then switch
					when getType(prop) is '[object String]' then element.innerHTML = prop
					when prop instanceof HTMLElement, prop instanceof DocumentFragment then element.appendChild prop
					else for key, value of prop when value isnt false then switch key
						when 'events' then element.addEventListener(key, value) for key, value of value
						else element.setAttribute(key, value)
				return element
			action = (elements, event, callback) ->
				if getType(elements) is '[object String]'  then elements = S elements
				unless getType(elements) in ['[object Array]', '[object NodeList]'] then elements = [elements]
				for element in elements then element.addEventListener event, callback
				return callback
			@S = (a, b) ->
				[root, query] = if b? then [a, b] else [document, a]
				results = root.querySelectorAll query
				return switch results.length
					when 0 then null
					when 1 then results[0]
					else        results
			
			@index     = 0
			@loadTimer = 0
			@imageZoom = 0
			@reset     = true
			@images    = []
			@files     = {}
			@totals    = {}
			@tags      = {}
			@sites     = {}
			@nextPages = {}
			@lastTags  = ''
			
			action window, 'load', ->
				action window,            'resize',            aspect
				action '#next img',         'load',            loaded
				action '#next img',        'error',            error
				action '#search',         'change',         -> search             @value, true
				action '#highest_rating', 'change',         -> search S('#search').value, true
				action '#go_to_first',     'click',         -> move(window.index = 0)
				action '#next',       'mouseenter',         -> S('#sidebar').classList.add 'hide' ; S('#search').blur()
				action '#sidebar',    'mouseenter',         -> if S('#settings').classList.contains 'hide' then S('#sidebar').classList.remove 'hide'
				action document,           'click', (event) -> unless S('input:focus')? then event.preventDefault()
				action document,     'contextmenu', (event) -> unless S('input:focus')? then event.preventDefault()
				action '#next',      'contextmenu', (event) -> move -1
				action '#next',            'click', (event) -> switch event.button
					when 0 then move 1
					when 1
						if S('#next img').classList.contains 'natural'
							S('#next img').classList.remove 'natural'
						else
							S('#next img').classList.remove 'tall', 'wide'
							S('#next img').classList.add    'natural'
						aspect()
				action '#open_settings', 'click', ->
					S('#settings').classList.remove 'hide'
					S('#sidebar' ).classList.add    'hide'
				action '#close_settings', 'click', ->
					S('#settings').classList.add    'hide'
					S('#sidebar' ).classList.remove 'hide'
				action document, 'keyup', (event) ->
					if S('input:focus')? then return
					amount = if event.shiftKey then 10 else 1
					switch event.keyCode
						when RIGHT, PERIOD then move amount
						when LEFT,  COMMA  then move amount * -1
						else return
					event.preventDefault()
				action '#interface_side',    'click', -> window.setInterfaceSide @value
				validNewSite = -> S('#new_site_add').disabled = (S('#new_site_address').value is '' or S('#new_site_type').selectedIndex is 0)
				action '#new_site_address',  'keyup', validNewSite
				action '#new_site_type',    'change', validNewSite
				action '#new_site_add', 'click', ->
					S('#new_site_add').disabled = true
					site = {}
					site[S('#new_site_address').value] =
						enabled: S('#new_site_enabled').checked
						type:    S('#new_site_type').value
					window.addSites site
					S('#new_site_address').value      = ''
					S('#new_site_enabled').checked    = true
					S('#new_site_type').selectedIndex = 0
					S('#sites_changes').classList.remove 'close'
					after 0.01, -> S('#sites_changes').classList.remove 'hide'
				action '#sites_apply', 'click', ->
					S('#sites_changes').classList.add 'hide'
					newSites = {}
					for row in document.querySelectorAll('#sites tbody tr')
						address = S(row, '.site_address').value
						unless address.match(/^[a-z0-9.+-]*[:]?\/\//i)? then address = 'http://' + address
						newSites[address] =
							enabled: S(row, '.site_enabled').checked
							type:    S(row, '.site_type').value
					localStorage.setItem('sites', JSON.stringify window.sites = newSites)
					S('#sites tbody').innerHTML = ''
					window.addSites window.sites
					window.search window.lastTags, true
				action '#sites_discard', 'click', ->
					S('#sites_changes').classList.add 'hide'
					S('#sites tbody').innerHTML = ''
					window.addSites window.sites
				action '.fade-in-out, .fade-out', 'transitionend', -> if @classList.contains 'hide' then @classList.add 'close'
				
				window.loadSettings()
				window.search '', true
			
			@updateTitle = -> document.title = "#{commas(index+1)}/#{commas(images.length)}: #{window.images[index] or 'Loading'} [#{window.imageZoom}%]"
			
			@move = (amount) ->
				index += amount
				# check if out of range
				if     (index < 0)               then index += images.length # go to end
				else if(index > images.length-1) then index -= images.length # go to start
				if files[images[index]] is undefined then error() ; return
				S('#prev img').src = S('#next img').src
				S('#prev img').setAttribute 'class', S('#next img').getAttribute 'class'
				S('#next').classList.remove 'forward', 'backward'
				S('#next').classList.add 'hide', if amount > 0 then 'forward' else 'backward'
				clearTimeout loadTimer
				if S('#next img').src isnt files[images[index]]
					S('#next img').src = files[images[index]]
					S('link[rel="shortcut icon"]').href = files[images[index]]
					S('#loading').classList.remove 'close'
					loadTimer = after 0.5, -> S('#loading').classList.remove 'hide'
				else
					S('#loading').classList.add 'hide'
				if images.length - index+1 < 50 then search S('#search').value, false
			
			@aspect = ->
				window.imageZoom = Math.floor S('#next img').height / S('#next img').naturalHeight * 100
				console.log "aspect() #{window.imageZoom}"
				updateTitle()
				windowAspect = innerHeight / innerWidth
				for image in [S('#next img'), S('#prev img')] when image.classList.contains('natural') is false
					imageAspect = image.naturalHeight / image.naturalWidth
					image.classList.remove 'tall', 'wide'
					image.classList.add if imageAspect > windowAspect then 'tall' else 'wide'
			
			@loaded = ->
				aspect()
				S('#next img').classList.remove 'pixel_art'
				tagLinks = document.createDocumentFragment()
				for tag in tags[images[index]] then do (tag) ->
					tagLinks.appendChild make 'li',
						make 'button', class: 'minus-tag', '-', title: 'Remove tag from current search', events: click: (event) ->
							search(S('#search').value.replace new RegExp("#{tag}\s*|\s*#{tag}", 'gi'), '')
						make 'button', class: 'plus-tag',  '+', title: 'Add tag to current search',      events: click: (event) ->
							search("#{S('#search').value} #{tag}")
						make 'button', class: 'tag', "#{tag} (#{commas totals[tag]})", title: 'Start new search with this tag', events: click: (event) ->
							search(tag)
					# While oekaki isn't strictly always done in pixel art style, most of it looks better with nearest neighbour interpolation
					if(tag in ['pixel_art', 'oekaki']) then S('#next img').classList.add 'pixel_art'
				S('#tags').innerHTML = ''
				S('#tags').appendChild tagLinks
				S('#save_image').href = S('#next img').src
				S('#save_image').download = images[index]
				
				clearTimeout loadTimer
				S('#next').classList.remove 'hide'
				S('#loading').classList.add 'hide'
			
			@error = ->
				# TODO: implement user friendly message about image failing to load
				clearTimeout loadTimer
				S('#loading').classList.add 'hide'
			
			@apis =
				'gelbooru':
					search: (site, tags, page) -> "#{site}/index.php?page=dapi&s=post&q=index&pid=#{page}&tags=#{tags}"
					results: (responses) ->
						results = new DOMParser().parseFromString(responses[0], 'text/xml')
						return images: results.getElementsByTagName('post'), count: results.getElementsByTagName('posts')[0].attributes.count.value
					image: (post) ->
						image =
							id:   post.attributes.md5.value
							tags: post.attributes.tags.value.replace(/^ | $|\n|\\/g,'').split /\s+/
							file: post.attributes.file_url.value
						image.tags.push "rating:" + switch post.attributes.rating.value
							when 'e' then 'explicit'
							when 'q' then 'questionable'
							when 's' then 'safe'
						return image
				'danbooru':
					search: (site, tags, page) -> [
						"#{site}/posts.json?limit=100&page=#{page+1}&tags=#{tags}" # searches default to only 20 results and pages are 1-indexed
						"#{site}/counts/posts.json?&tags=#{tags}" # search results don't include a total so we have to make a second request ¬_¬
						]
					results: (responses) -> return images: JSON.parse(responses[0]), count: JSON.parse(responses[1]).counts.posts
					image: (post) ->
						image =
							id:   post.md5
							tags: post.tag_string.replace(/^ | $|\n|\\/g,'').split /\s+/
							file: post.file_url
						image.tags.push "rating:" + switch post.rating
							when 'e' then 'explicit'
							when 'q' then 'questionable'
							when 's' then 'safe'
						return image
			
			@getResults = (site, tags, page) ->
				api  = apis[sites[site].type]
				urls = api.search(site, tags, page)
				if getType(urls) is '[object String]' then urls = [urls]
				ready = 0
				responses = new Array urls.length
				for url, index in urls then do (site, tags, page, url, index) ->
					xhr  = new XMLHttpRequest()
					xhr.open 'GET', url, true
					xhr.onload = ->
						responses[index] = @responseText
						ready++
						if ready isnt urls.length then return
						results = api.results responses
						if doreset = window.reset and results.images.length > 0
							window.reset = false
							window.images = []
						offset = results.images.length * (page + 1)
						remaining = results.count - offset
						window.nextPages[site] = if remaining > 0 then page + 1 else -1
						
						added  = 0
						merged = 0
						for image in results.images then do (image = api.image(image)) ->
							unless (image.file? and image.id?) then return # no imade url or id? then we bail
							unless image.file.match(/^[a-z0-9.+-]*[:]?\/\//i)? then image.file = "#{site}/#{image.file}"
							image.tags.push "site:#{site}"
							if window.tags[image.id]?
								image.tags = image.tags.concat window.tags[image.id]
								merged++
							else
								window.tags[image.id] = []
								window.files[image.id] = image.file
								added++
							if window.images.indexOf(image.id) is -1 then window.images.push image.id
							for tag, i in image.tags when window.tags[image.id].indexOf(tag) is -1
								window.tags[image.id].push tag
								# we perform a type check instead of just existence to avoid type coercion with builtin types, e.g. like 'constructor'
								window.totals[tag] = unless isNaN window.totals[tag] then window.totals[tag] + 1 else 1
						
						if doreset then move window.index = 0
						else updateTitle()
						
						NAME  = 'font-weight: bold; color: gray'
						VALUE = 'font-weight: normal'
						console.groupCollapsed "Made request to: #{urls[0]}"
						console.log "%c Site:%c #{site}", NAME, VALUE
						console.log "%c Type:%c #{sites[site].api}", NAME, VALUE
						console.log "%c Tags:%c #{tags}", NAME, VALUE
						console.log "%cReset:%c #{doreset}", NAME, VALUE
						console.group "Page:"
						console.log "%cThis:%c #{page}", NAME, VALUE
						console.log "%cNext:%c #{nextPages[site]}", NAME, VALUE
						console.groupEnd()
						console.group "Images:"
						console.log "%c Total:%c #{added+merged}", NAME, VALUE
						console.log "%c Added:%c #{added}", NAME, VALUE
						console.log "%cMerged:%c #{merged}", NAME, VALUE
						console.groupEnd()
						console.group "Results:"
						console.log "%c    Count:%c #{results.count}", NAME, VALUE
						console.log "%c   Offset:%c #{offset}", NAME, VALUE
						console.log "%cRemaining:%c #{if remaining > 0 then remaining else 0}", NAME, VALUE
						console.groupEnd()
						console.groupEnd()
						clearTimeout loadTimer
					xhr.send()
			
			@search = (tags='', reset) ->
				window.reset = reset or tags isnt window.lastTags
				S('#search').value = window.lastTags = tags
				tags = switch S('#highest_rating').value
					when 'safe'         then  "rating:safe "     + tags
					when 'questionable' then "-rating:explicit " + tags
					when 'explicit'     then                       tags
				if window.reset
					S('#loading').classList.remove 'close'
					do -> S('#loading').classList.remove 'hide'
					window.nextPages = {}
				for site of sites when sites[site].enabled then do (site) ->
					page = window.nextPages[site] or 0
					if page isnt -1 then getResults site, tags, page
			
			@loadSettings = ->
				window.interfaceSide = localStorage.getItem('interface_side') or 'left'
				S("#interface_side").value = window.interfaceSide
				window.setInterfaceSide      window.interfaceSide
				window.sites = JSON.parse(localStorage.getItem('sites')) or
					'http://gelbooru.com':
						enabled: true
						type: 'gelbooru'
					'https://danbooru.donmai.us':
						enabled: false
						type: 'danbooru'
					'http://tbib.org':
						enabled: false
						type: 'gelbooru'
					'http://rule34.xxx':
						enabled: false
						type: 'gelbooru'
				window.addSites window.sites
			
			@addSites = (sites) ->
				newSites = document.createDocumentFragment()
				typeList = document.createDocumentFragment()
				showChanges = -> S('#sites_changes').classList.remove 'close' ; after 0.01, -> S('#sites_changes').classList.remove 'hide'
				for type of window.apis then typeList.appendChild make 'option', value: type, type
				for site of sites then do (address = site, site = sites[site], typeList = typeList.cloneNode true) ->
					typeList.querySelector("""option[value="#{site.type}"]""").setAttribute 'selected', true
					newSites.appendChild make 'tr',
						make 'td', make 'input',  class: 'site_enabled', type: 'checkbox', checked: site.enabled or false, events: click:  showChanges
						make 'td', make 'input',  class: 'site_address', type: 'url',      value:   address or '',         events: keyup:  showChanges
						make 'td', make 'select', class: 'site_type',    typeList,         value:   site.type,             events: change: showChanges
						make 'td', make 'button', class: 'site_remove', 'Remove', events: click: ->
							row = @parentElement.parentElement
							row.parentElement.removeChild row
							showChanges()
				S('#sites tbody').appendChild newSites
			
			@setInterfaceSide = (side) ->
				localStorage.setItem('interface_side', window.interfaceSide = side)
				for side in ['right', 'left']
					if window.interfaceSide is side
						S('#sidebar' ).classList.add side
						S('#settings').classList.add side
					else
						S('#sidebar' ).classList.remove side
						S('#settings').classList.remove side
	body ->
		div id: 'prev', class: 'frame', ->
			img class: 'center'
			svg id: 'loading', class: 'center fade-in-out', xmlns: 'http://www.w3.org/2000/svg', width: '25%', height: '25%', viewBox: '0 0 32 32', ->
				path opacity: 0.25,  fill: 'black', d: 'M16 0 A16 16 0 0 0 16 32 A16 16 0 0 0 16 0 M16 4 A12 12 0 0 1 16 28 A12 12 0 0 1 16 4'
				path fill: 'white', d: 'M16 0 A16 16 0 0 1 32 16 L28 16 A12 12 0 0 0 16 4z', ->
					animateTransform attributeName: 'transform', type: 'rotate', from: '0 16 16', to: '360 16 16', dur: '0.8s', repeatCount: 'indefinite'
		div id: 'next', class: 'frame fade-in hide', ->
			img class: 'center'
		div id: 'settings', class: 'hide', ->
			button id: 'close_settings', title: 'Close settings', 'Settings'
			h1 'Interface'
			p ->
				label for: 'interface_side', 'Display on the '
				select id: 'interface_side', ->
					option value: 'left',  'left'
					option value: 'right', 'right'
			h1 'Sites'
			table id: 'sites', ->
				thead ->
					th()
					th 'Address'
					th 'Type'
					th()
				tbody()
				tfoot ->
					tr id: 'new_site', ->
						td -> input id: 'new_site_enabled', type: 'checkbox', checked: true
						td -> input id: 'new_site_address', type: 'url', placeholder: 'example.com'
						td -> select id: 'new_site_type', ->
							option disabled: true, selected: true, 'Pick...'
							option value: 'gelbooru', 'gelbooru'
							option value: 'danbooru', 'danbooru'
						td -> button id: 'new_site_add', disabled: true, 'Add'
			div id: 'sites_changes', class: 'fade-in-out hide close', ->
				label 'Changes to sites: '
				button id: 'sites_apply',   '✓ Apply'
				button id: 'sites_discard', '✕ Discard'
		div id: 'sidebar', class: 'hide', ->
			small id: 'info-version', "untitled *booru-viewer #{version}"
			button id: 'open_settings', href: '', title: 'Open settings', 'Settings'
			p ->
				label for: 'search', 'Search: '
				input id: 'search', type: 'search', placeholder: 'tag_a tag_b tag_c'
			p ->
				label for: 'highest_rating', 'Highest Rating: '
				select id: 'highest_rating', ->
					option value: 'explicit',             'Explicit'
					option value: 'questionable',         'Questionable'
					option value: 'safe', selected: true, 'Safe'
			p -> button id: 'go_to_first', title: 'Return to the first image in the current search', 'Go back to first image'
			ul id: 'tags'
			p -> a id: 'save_image', href: '', title: 'Open a file dialog to save the current image permantly', download: 'true', 'Save this image to disk'
		div id: 'thumbnails', ->
			img()
