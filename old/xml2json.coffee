#!/usr/bin/env coffee
if process.argv.length < 3
	console.error """
		You silly goose!
		Usage: #{process.argv[1]} <*.xml>... > output.js
	"""
	process.exit 1

fs = require 'fs'

pretty = (num) -> num.toString().replace /\B(?=(\d{3})+(?!\d))/g, ","

tags   = {}
totals = {}
total  = 0

results = process.argv.splice 2
for result, i in results
	console.error "Working on #{result} (#{i+1}/#{results.length})"
	posts = fs.readFileSync(result).toString().split('<post ').splice 1
	console.error "\tFound: #{pretty posts.length}"
	added  = 0
	merged = 0
	for post in posts
		attributes = post.split /"[ ]*/
		uri  = attributes[attributes.indexOf('file_url=')+1].split '/'
		file = uri[uri.length-1]
		list = attributes[attributes.indexOf('tags=')+1].replace(/^ | $|\n|\\/g,'').split /\s+/
		list.push "rating:" + switch attributes[attributes.indexOf('rating=')+1]
			when 'e' then 'explicit'
			when 'q' then 'questionable'
			when 's' then 'safe'
		list.push "mimetype:" + switch /\.(\w*)$/.exec(file)[1] # get file extension, but return without "."
			when 'png' then 'image/png'
			when 'gif' then 'image/gif'
			when 'bmp' then 'image/x-ms-bmp'
			when 'jpg', 'jpeg' then 'image/jpeg'
		list.push "host:#{uri[2]}"
		if tags[file]?
			list = list.concat tags[file]
			merged++
		else
			tags[file] = []
			added++
		for tag, index in list when tags[file].indexOf(tag) is -1
			tags[file].push tag
			# we perform a type check instead of just existence to avoid type cohersion with builtin types, e.g. like 'constructor'
			totals[tag] = unless isNaN totals[tag] then totals[tag] + 1 else 1
	total += added
	console.error """
			Added: #{pretty added}
			Merged: #{pretty merged}
		Total: #{pretty total}
		Memory used: #{pretty (process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)}Mb
	"""

done = 0
percent = 0
console.error "Finally writing JSON..."
console.log "var tags = {"
overwrite = (num) -> "\x1B[1K\x1B[#{num}D" # erase current line and move cursor back <num> characters
# we can't use JSON.stringify here as it tries to store the entire result into memory, also we can now have progress info
for image of tags
	process.stdout.write """"#{image}": ["#{tags[image].join '","'}"],"""
	new_percent = parseFloat ((done++ / total) * 100).toFixed 2
	if new_percent > percent
		percent = new_percent
		process.stderr.write "#{overwrite 6}#{percent}%"
console.log """
	}
	var images = Object.keys(tags)
	var totals = {
"""
for tag of totals
	console.log """"#{tag}": #{totals[tag]},"""
console.log "}"

console.error "#{overwrite 6}All done! :)"
