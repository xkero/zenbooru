package org.gamemage.zenbooru;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.os.Environment;
import android.app.Activity;
import android.os.Bundle;
import android.net.Uri;
import android.content.Context;
import android.util.Base64;
import android.util.Log;

import android.widget.Toast;
import android.webkit.WebView;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.ConsoleMessage;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.media.MediaScannerConnection;

public class main extends Activity
	{
	public WebView wv;
	public Context ctx;
	private void scanFile(String path) { MediaScannerConnection.scanFile(main.this, new String[] { path }, null, null); }
	@Override
	public void onCreate(Bundle savedInstanceState)
		{
		/*
		Here we move our webview's cache to our app's cache via a symlink to fix a bug where the webview stores it's cache in our data folder.
		This is an issue as the user can't then clear this cache without deleting the whole data folder and losing their settings as well.
		This was fixed (https://codereview.chromium.org/722413002/) about 4 months ago (at the time of writing), but users on android < 5.0 will never see that fix.
		*/
		File symlink = new File(getDir("webview", 0).getAbsolutePath() + "/Cache");
		if(symlink.exists() == false)
			{
			try { Runtime.getRuntime().exec(new String[] {"ln", "-s", getCacheDir().getAbsolutePath(), symlink.getAbsolutePath()}); }
			catch (IOException error) { error.printStackTrace(); }
			}
		ctx = getApplicationContext();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		wv = (WebView) findViewById(R.id.webview);
		wv.setBackgroundColor(0x00000000); // Make background transparent to prevent white flash on startup
		//wv.setWebContentsDebuggingEnabled(true); // requires sdk 4.4+
		wv.setWebChromeClient(new WebChromeClient()
			{
			public boolean onConsoleMessage(ConsoleMessage cm)
				{
				Log.d("Zenbooru", cm.message() + " -- From line " + cm.lineNumber() + " of " + cm.sourceId());
				return true;
				}
			});
		class JSInterface
			{
			@JavascriptInterface
			public void download(String name, String base64)
				{
				File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
				dir.mkdirs();
				Log.d("Zenbooru", "Filename: " + name + "\nFolder: " + dir.getAbsolutePath() + "\nBase64: " + base64);
				byte[] data = Base64.decode(base64.split(",")[1], 0);
				try
					{
					FileOutputStream file = new FileOutputStream(new File(dir + "/" + name));
					file.write(data);
					file.close();
					Toast.makeText(ctx, "Saved to " + dir.getAbsolutePath(), Toast.LENGTH_SHORT).show();
					scanFile(dir + "/" + name);
					}
				catch (FileNotFoundException error) { error.printStackTrace(); }
				catch (IOException           error) { error.printStackTrace(); }
				}
			}
		wv.addJavascriptInterface(new JSInterface(), "android");
		wv.setDownloadListener(new DownloadListener()
			{
			public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength)
				{
				Log.d("Zenbooru", "Download: " + url);
				wv.loadUrl("javascript:"
				          +"var reader = new window.FileReader();"
				          +"reader.readAsDataURL(window.currentFile.blob);"
				          +"reader.onloadend = function() {android.download(S('#save_file').download, reader.result)}");
				}
			});
		WebSettings ws = wv.getSettings();
		ws.setJavaScriptEnabled(true);
		ws.setDomStorageEnabled(true);
		ws.setAllowUniversalAccessFromFileURLs(true);
		ws.setMediaPlaybackRequiresUserGesture(false);
		
		wv.loadUrl("file:///android_asset/index.html");
		}
	}
