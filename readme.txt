Zenbooru (全ボール) is a client for image boards implementing the Gelbooru v0.2, Danbooru (v1 & v2) and Moebooru APIs.
Project homepage: https://bitbucket.org/xkero/zenbooru

To build:
sudo npm install -g coffee-script # needed to run make script
npm install teacup stylus # install build dependencies
coffee ./make.coffee
./platforms/portable/start.anyOS.bat

License: GPLv3 (http://www.gnu.org/copyleft/gpl.html#content)
