@TRI_DELIMIT      = /\B(?=(\d{3})+(?!\d))/g
@EXTRA_WHITESPACE = /^\s+|\s+$|\n+/g
@END_WHITESPACE   = /\s$/
@URI_SCHEME       = /^[a-z0-9.+-]*[:]?\/\//i
@ESCAPE_FOR_REGEX = /[|\\{}()[\]^$+*?.]/g
@PIXIV_LINK       = /\/\/\w+\.pixiv.net\/([\w-]+\/)+(\d+).*/g

# log levels: 0 = no log output, 1 = only expanded, 2 = everything
@LOG_LEVEL = 0
@log = (a, b, c) -> if window.LOG_LEVEL > 0
	[title, expanded, object] = if c? then [a, b, c] else [a, true, b]
	if window.LOG_LEVEL is 1 and expanded is false then return
	if expanded then console.group title else console.groupCollapsed title
	sub = (object) ->
		for key, value of object
			if getType(value) is '[object Object]'
				console.group "#{key}:"
				sub value
			else console.log "%c #{key}:%c #{value}", 'font-weight: bold; color: gray', 'font-weight: normal'
		console.groupEnd()
	sub object
	console.groupEnd()

nextFrame = (callback)      -> window.requestAnimationFrame -> window.requestAnimationFrame callback # Is this really the only way of doing this?
after     = (ms, callback)  -> setTimeout callback, ms*1000
thousands = (num)           -> String(num).replace window.TRI_DELIMIT, ","
getType   = (object)        -> Object::toString.call object
make      = (tag, props...) ->
	element = document.createElement tag
	for prop in props then switch
		when getType(prop) is '[object String]' then element.innerHTML = prop
		when prop instanceof HTMLElement, prop instanceof DocumentFragment then element.appendChild prop
		else for key, value of prop when value isnt false then switch key
			when 'events'    then for event, callback of value then element.addEventListener event, callback
			when 'innerText' then element.innerText = value
			else element.setAttribute(key, value)
	return element
doFor = (elements, args..., doWhat) ->
	if getType(elements) is '[object String]' then elements = S elements
	unless getType(elements) in ['[object Array]', '[object NodeList]'] then elements = [elements]
	for element in elements then doWhat.apply element, args
action = (elements, event, callback) ->
	doFor elements, event, callback, (event, callback) -> @addEventListener event, callback
	return callback
@true = true ; @false = false # hack so if state is boolean, window[state] will still work
@show = (elements, state=true, callee) -> doFor elements, ->
	if window.LOG_LEVEL is 2 then console.log 'start', window[state], callee
	if window[state] then @classList.remove 'close'
	else
		@classList.add 'hide'
		@addEventListener 'transitionend', ->
			@removeEventListener 'transitionend', arguments.callee
			if window.LOG_LEVEL is 2 then console.log 'trans', window[state], callee
			unless window[state] then @classList.add 'hide', 'close'
	nextFrame =>
		if window.LOG_LEVEL is 2 then console.log 'frame', window[state], callee
		if window[state] then @classList.remove 'hide'
		else                  @classList.add    'hide', 'close'
@S = (a, b) ->
	[root, query] = if b? then [a, b] else [document, a]
	results = root.querySelectorAll query
	return switch results.length
		when 0 then null
		when 1 then results[0]
		else        results
@getTags     = (string) -> if string? and string isnt '' then string.replace(window.EXTRA_WHITESPACE, '').split /\s+/ else []
@regexEscape = (string) -> string = string.replace window.ESCAPE_FOR_REGEX, '\\$&'

window.URL ?= window.webkitURL # support old webkit (e.g. Android <= 4.2, Chrome <= 22)
Element.prototype.scrollTo ?= (x, y) -> @scrollLeft  = x ; @scrollTop  = y
Element.prototype.scrollBy ?= (x, y) -> @scrollLeft += x ; @scrollTop += y
