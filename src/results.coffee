action window, 'load', ->
	action '#messages',        'transitionend', -> if @classList.contains 'hide' then @innerHTML = ''
	action '#loading-results', 'transitionend', -> if window.resultsDone >= window.resultsTotal then after window.messageDelay, ->
		S('#loading-results').classList.add 'hide'
		S('#messages').classList.add        'hide'

@results = []
@files   = {}
@apis    = {}
@tags    =
	'rating:safe':         total: 0
	'rating:questionable': total: 0
	'rating:explicit':     total: 0

# Not a full api, just used as a base for other gel/moe/danbooru xml style apis
@apis.xmlbase =
	results: (response) ->
		results = new DOMParser().parseFromString response, 'text/xml'
		meta    = results.getElementsByTagName('posts')[0]
		count   = parseInt meta.getAttribute 'count'
		offset  = parseInt meta.getAttribute 'offset'
		files   = results.getElementsByTagName 'post'
		return files: files, more: offset+files.length < count
	file: (post) ->
		get = (prop) -> post.getAttribute prop
		file =
			checksum: get 'md5'
			id:       get 'id'
			tags:     window.getTags get 'tags'
			original: get 'file_url'
			sample:   get 'sample_url'
			thumb:    get 'preview_url'
			source:   get 'source'
			height:   get 'height'
			width:    get 'width'
			created:  get 'created_at'
			uploader: get 'creater_id'
			rating:   switch get 'rating'
				when 'e' then 'explicit'
				when 'q' then 'questionable'
				when 's' then 'safe'
		return file
@apis['booru.org'] =
	search: (site, tags, page) -> "#{site}/index.php?page=post&s=list&pid=#{page*20}&tags=#{if tags is '' then 'all' else tags}"
	results: (response) ->
		results = new DOMParser().parseFromString response, 'text/html'
		return files: results.querySelectorAll('.thumb a'), more: results.querySelector('a[alt="last page"]')?
	file: (post) ->
		image = post.children[0]
		url = image.getAttribute 'src'
		tags = image.getAttribute('title').toLowerCase()
		file =
			checksum: url.split('_')[1].split('.')[0]
			id:       post.getAttribute 'id'
			tags:     window.getTags tags.replace /rating:\w+/, ''
			original: url.replace('thumbs','img').replace('thumbnails','images').replace 'thumbnail_',''
			sample:   undefined
			thumb:    image.getAttribute 'src'
			source:   undefined
			created:  undefined
			page:     post.getAttribute 'href'
			rating:   tags.match(/rating:(\w+)/)?[1]
		return file
@apis['gelbooru'] =
	search: (site, tags, page, size=100) -> "#{site}/index.php?page=dapi&s=post&q=index&limit=#{size}&pid=#{page}&tags=#{tags}"
	results: @apis.xmlbase.results
	file: (post) ->
		file = window.apis.xmlbase.file post
		file.created = new Date file.created
		file.page = "/index.php?page=post&s=view&id=#{file.id}"
		return file
@apis['moebooru'] =
	search: (site, tags, page, size=100) -> "#{site}/post.xml?limit=#{size}&page=#{page+1}&tags=#{tags}"
	results: @apis.xmlbase.results
	file: (post) ->
		file = window.apis.xmlbase.file post
		file.created = (d = new Date(); d.setTime(parseInt(file.created)*1000); d)
		file.page = "/post/show/#{file.id}" if file.id?
		return file
@apis['danbooru v1'] =
	search: (site, tags, page, size=100) -> "#{site}/post/index.xml?limit=#{size}&page=#{page+1}&tags=#{tags}"
	error: (response) -> try (new DOMParser().parseFromString(response, 'text/xml')).firstChild.attributes.reason.value catch then return
	results: @apis.xmlbase.results
	file: (post) ->
		file = window.apis.xmlbase.file post
		file.created = new Date file.created
		file.page = "/post/show/#{file.id}" if file.id?
		return file
@apis['danbooru v2'] =
	search: (site, tags, page, size=100) -> "#{site}/posts.json?limit=#{size}&page=#{page+1}&tags=#{tags}"
	results: (response, size=100) ->
		files = JSON.parse response
		return files: files, more: files.length is size
	file: (post) ->
		file =
			checksum: post.md5
			id:       post.id
			tags:     window.getTags post.tag_string
			original: post.file_url
			sample:   post.large_file_url
			thumb:    post.preview_file_url
			source:   post.source
			uploader: post.uploader_name
			created:  new Date post.created_at
			height:   post.image_height
			width:    post.image_width
			notes:    "/notes.json?search[post_id]=#{post.id}&group_by=note&limit=100" if post.last_noted_at?
			page:     "/posts/#{post.id}" if post.id?
			rating:   switch post.rating
				when 'e' then 'explicit'
				when 'q' then 'questionable'
				when 's' then 'safe'
		for type in ['artist', 'character', 'copyright', 'general']
			for tag in window.getTags post["tag_string_#{type}"]
				window.tags[tag] ?= total: 0
				window.tags[tag].type = type
		return file
	error: (response) -> try JSON.parse(response).message catch then return
	notes: (response) -> JSON.parse response
@apis['sankaku complex'] =
	search: (site, tags, page) -> "#{site}/post/index.content?tags=#{tags}&page=#{page+1}"
	results: (response) ->
		results = new DOMParser().parseFromString response, 'text/html'
		files = S results, 'img.preview'
		return files: files, more: files.length > 0
	file: (post) ->
		get = (prop) -> post.attributes[prop]?.value
		thumb = get('src').replace /^\/\//, 'https://'
		parts = thumb.split '/'
		name = parts[parts.length-1]
		file =
			checksum: name.split('.')[0]
			id:       undefined
			tags:     window.getTags get('title').toLowerCase().replace /size:[\d]+x[\d]+/g, ''
			original: get 'href'
			sample:   undefined
			thumb:    thumb
			source:   undefined
			created:  undefined
		return file
@apis['shimmie 2 danbooru'] =
	search: (site, tags, page, size) -> "#{site}/api/danbooru/find_posts/index.xml?tags=#{tags}&page=#{page+1}&limit=#{size}"
	results: @apis.xmlbase.results
	file: (post) ->
		file = window.apis.xmlbase.file post
		file.uploader = post.getAttribute 'author'
		file.created  = new Date file.created
		file.page     = "/post/view/#{file.id}" if file.id?
		file.rating   = switch post.getAttribute 'rating'
			when 'e' then 'explicit'
			when 'q' then 'questionable'
			when 's' then 'safe'
		return file

@getResults = (api, site, tags, page, size, callback) ->
	url = api.search site, tags, page, size
	xhr = new XMLHttpRequest()
	xhr.open 'GET', url, true
	xhr.onerror = ->
		error = status: 'connection error'
		callback error, site, tags
	xhr.onload = ->
		if @status isnt 200 then error = status: @status, message: api.error? @responseText
		else
			try rawResults = api.results @responseText, size
			catch error
				error = status: 'results error', stack: error.stack
		window.log "Made request to: #{url}", false,
			Site: site
			Tags: tags
			Page: page
			Many: rawResults?.files.length or 0
			More: rawResults?.more
		callback error, site, tags, rawResults
	xhr.send()

@mergeResults = (error, site, tags, rawResults) ->
	if error?
		window.resultsProgress site, error.status, error.message
		if error.stack? then console.log error.stack
		return
	window.resultsProgress site
	reset = window.reset
	window.reset = false
	
	added  = 0
	merged = 0
	for file in rawResults.files then do (file = window.apis[window.sites[site].type].file file) ->
		unless file.checksum? then return
		for url in ['original', 'sample', 'thumb', 'notes', 'page'] when file[url]?
			unless file[url].match(window.URI_SCHEME)? then file[url] = "#{site}#{if file[url][0] is '/' then '' else '/'}#{file[url]}"
		if file.sample is file.original then file.sample = undefined
		if window.files[file.checksum]?
			window.files[file.checksum].sample ?= file.sample
			ratings = undefined: -1, safe: 0, questionable: 1, explicit: 2
			if ratings[window.files[file.checksum].rating] < ratings[file.rating]
				window.files[file.checksum].rating = file.rating
				window.tags["rating:#{window.files[file.checksum].rating}"]?.total--
				window.tags["rating:#{file.rating}"].total++
			merged++
		else
			window.files[file.checksum] =
				checksum: file.checksum
				tags:     []
				original: file.original
				sample:   file.sample or undefined
				thumb:    file.thumb
				height:   file.height or 0
				width:    file.width  or 0
				rating:   file.rating or undefined
				results:  {}
			window.tags["rating:#{file.rating}"]?.total++
			added++
		for tag in file.tags.concat(window.includeTags) when window.files[file.checksum].tags.indexOf(tag) is -1
			window.files[file.checksum].tags.push tag
			# we perform a type check instead of just existence to avoid type coercion with builtin types, e.g. like 'constructor'
			if isNaN parseInt window.tags[tag]?.total
				window.tags[tag] = total: 1
			else window.tags[tag].total++
		window.files[file.checksum].results[site] = file
		if window.results.indexOf(file.checksum) is -1 and file.original? then window.results.push file.checksum
	
	window.moreResults[site] = rawResults.more
	
	if added+merged > 0
		if reset
			window.addThumbs true
			move window.index = 0
		else
			window.move(window.index = 0) unless window.index?
			window.updateResultsCounter()
			window.updateInfo()
			window.updateTitle()
			window.addThumbs()
	else
		if window.resultsDone >= window.resultsTotal and window.results.length is 0
			display 'no results'
			delete window.index
		else window.reset = reset
	
	window.log "Merged new results", false,
		Search:
			Site:   site
			Tags:   tags
			Reset:  reset
		Results:
			Total:  rawResults.files.length
			Added:  added
			Merged: merged

@resultsProgress = (site, status, message) ->
	window.resultsDone++
	S('#loading-results').classList.remove 'hide'
	S('#loading-results').style.width = Math.round((window.resultsDone / window.resultsTotal) * 100) + '%'
	
	window.siteStatus site, status, message

@siteStatus = (site, status, message) ->
	site_status = S """#sites [data-site="#{site}"] .site_status"""
	
	window.messageDelay = 5
	info = switch status
		when undefined
			icon: '😄', type: 'okay',
			tip:  "A-O-kay!"
		when 400, 404, 'results error', 'unsupported'
			window.enableSite site, false
			window.showPanel '#settings', true, true
			icon: '😕', type: 'error',
			text: "#{site}: Not an image board or unsupported. Site has been disabled, please check address is correct before re-enabling.",
			tip:  "Unsupported"
		when 418
			window.enableSite site, false
			window.showPanel '#settings', true, true
			icon: '☕', type: 'error',
			text: """#{site}: is actually a <a href="http://tools.ietf.org/html/rfc2324">teapot</a>, not an image board. Site has been disabled, please correct before re-enabling.""",
			tip:  "Is actually a teapot, not an image board"
		when 429
			icon: '😠', type: 'warn',
			text: "#{site}: claims we have sent too many requests.",
			tip:  "Sent too many requests"
		when 500
			if message?
				icon: '😱', type: 'info',
				text: "#{site}: encountered an error, it said:<br>#{message}",
				tip:  "Server error, it said:\n#{message}"
			else
				icon: '😱', type: 'info',
				text: "#{site}: encountered an error.",
				tip:  "Server error"
		when 502, 503, 504
			icon: '😷', type: 'warn',
			text: "#{site}: overwhelmed or down for maintenance.",
			tip:  "Overwhelmed or down for maintenance."
		when 'connection error'
			icon: '😵', type: 'error',
			text: "#{site}: couldn't contact server."
			tip:  "Couldn't contact."
		when 'results none'
			icon: '😶', type: 'info',
			text: "All sites returned no results."
		else
			icon: '😕', type: 'error',
			text: "#{site}: returned an unhandled error (HTTP #{status}).",
			tip:  "HTTP #{status}"
	if info.type isnt 'okay'
		S('#loading-results').classList.add info.type
		S('#messages').appendChild make 'p', """<big class="emoji">#{info.icon}</big> #{info.text}"""
		show '#messages', true
		after window.messageDelay, -> S('#messages').classList.add 'hide'
	
	site_status.innerHTML = "#{info.icon} #{info.tip}"
	site_status.setAttribute 'class', "site_status emoji #{info.type}"
