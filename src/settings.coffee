@SETTINGSVERSION = '2'

action window, 'load', ->
	window.loadSettings()
	action '#open_settings',    'click', -> window.showPanel '#settings', true
	action '#close_settings',   'click', -> window.showPanel '#settings', false ; window.showPanel '#sidebar', true
	action '#interface_side',  'change', -> window.setInterfaceSide @value
	action '#new_site_address','change', -> @blur()
	action '#new_site_address',  'blur', -> if @value isnt ''
		address = unless @value.match(window.URI_SCHEME)? then 'http://' + @value else @value
		@value = ''
		site = {}
		site[address] = window.sites[address] = enabled: false
		window.saveSites()
		window.addSites site
		window.newSite address
	action '#third_party_site_disclaimer_dismiss', 'click', -> window.dismissThirdPartySiteDisclaimer()
	window.search '', true
@loadSettings = ->
	# from v0.1 to v0.2.6 the settings format had no version so we set it to '1' if settings are present without a version
	version = if localStorage.getItem('sites')? then localStorage.getItem('version') or '1' else window.SETTINGSVERSION
	window.setInterfaceSide localStorage.getItem('interface_side') or 'left'
	unless localStorage.getItem('default_file_source')? then localStorage.setItem 'default_file_source', 'sample'
	if localStorage.getItem('dismiss_third_party_site_disclaimer') is 'true' then window.dismissThirdPartySiteDisclaimer()
	window.sites = JSON.parse(localStorage.getItem('sites')) or window.DEFAULT_SITES
	# what I originaly called 'danbooru v1' was actually the 'moebooru' api, at the time I thought they were the same, turns out they aren't >_<
	if version is '1'
		for site of window.sites when window.sites[site].type is 'danbooru v1' then window.sites[site].type = 'moebooru'
		localStorage.setItem 'sites', JSON.stringify window.sites
	localStorage.setItem 'version', window.SETTINGSVERSION
	window.addSites window.sites

@setInterfaceSide = (side) ->
	localStorage.setItem 'interface_side', S("#interface_side").value = window.interfaceSide = side
	for side in ['right', 'left']
		if window.interfaceSide is side
			S('#sidebar' ).classList.add side
			S('#settings').classList.add side
		else
			S('#sidebar' ).classList.remove side
			S('#settings').classList.remove side

@dismissThirdPartySiteDisclaimer = ->
	localStorage.setItem 'dismiss_third_party_site_disclaimer', 'true'
	S('#third_party_site_disclaimer').classList.add 'hide'

@detectSite = (site, callback) ->
	order = [
		'danbooru v2'
		'moebooru'
		'danbooru v1'
		'gelbooru'
		'shimmie 2 danbooru'
		'booru.org'
	]
	test = (i) ->
		if api = window.apis[order[i]] then window.getResults api, site, '', 0, 1, (error, site, tags, rawResults) ->
			if error?
				if error.status is 'connection error' then callback error
				else test i+1
			else callback undefined, order[i]
		else callback status: 'unsupported'
	test 0

@addSites = (sites) ->
	newSites = document.createDocumentFragment()
	for site of sites then do (address = site, site = sites[site]) ->
		newSite = make 'div', class: 'site', 'data-site': address,
			make 'input',  class: 'site_enabled', type: 'checkbox', checked: site.enabled or false, events: click: ->
				window.enableSite address, @checked
				window.saveSites()
			make 'input',  class: 'site_address', type: 'url', value: address or '', events:
				change: -> @blur()
				blur: ->
					delete window.sites[address]
					delete window.moreResults[address]
					@parentElement.setAttribute 'data-site', @value
					@setAttribute 'value', @value
					window.saveSites()
					window.newSite @value
			make 'button', class: 'site_remove', '✕', events: click: ->
				site = @parentElement
				site.parentElement.removeChild site
				delete window.sites[address]
				window.saveSites()
			make 'div', class: 'site_status'
		newSites.insertBefore newSite, newSites.firstChild
	S('#sites').insertBefore newSites, S('#new_site').nextSibling

@newSite = (site) ->
	site_status = S """#sites [data-site="#{site}"] .site_status"""
	site_status.innerHTML = '<div class="loading"></div>'
	window.detectSite site, (error, type) ->
		if error?
			window.siteStatus site, error.status
		else
			window.sites[site] = type: type
			window.enableSite site
			window.saveSites()
			window.siteStatus site

@enableSite = (site, enable=true) ->
	window.sites[site].enabled = S("#sites [data-site='#{site}'] .site_enabled").checked = enable
	tags = window.previousSearch + switch S('#highest_rating').value
			when 'safe'         then ' rating:safe'
			when 'questionable' then ' -rating:explicit'
			else                     ''
	if enable then for page in [0..window.resultsPage-1]
		if window.moreResults[site] ?= true then getResults window.apis[window.sites[site].type], site, tags, page, 100, window.mergeResults
	window.saveSites()

@saveSites = -> localStorage.setItem 'sites', JSON.stringify window.sites

