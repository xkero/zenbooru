action window, 'load', ->
	[RIGHT, PERIOD, LEFT, COMMA] = [39, 190, 37, 188]
	action document, 'keyup', (event) -> unless S('input:focus')?
		amount = if event.shiftKey then 10 else 1
		switch event.keyCode
			when RIGHT, PERIOD then move amount
			when LEFT,  COMMA  then move amount * -1
			else return
		event.preventDefault()
	action '#gestures' , 'mouseenter', (event) ->
			window.showPanel '#sidebar', false
			window.showPanel '#bottombar', false
	action '#gestures' , 'contextmenu', -> move -1
	# Prevent Window's annoying middle-click scroll orb thingy
	action '#gestures' , 'mousedown', (event) -> if event.button is 1 then event.preventDefault()
	action '#gestures' , 'click',     (event) -> if event.button is 1 then window.zoom 0, event.x, event.y
	mc = new Hammer S '#gestures'
	swipe = new Hammer.Swipe threshold: 0, direction: Hammer.DIRECTION_ALL
	pan   = new Hammer.Pan()
	pinch = new Hammer.Pinch()
	swipe.recognizeWith pan
	mc.add [pinch, swipe, pan]
	mc.on 'swipeup',       -> unless S('.view.natural')?
		window.showPanel '#bottombar'
		window.updateThumbs()
	mc.on 'swipedown',     -> window.showPanel '#bottombar'
	mc.on 'swipeleft',     -> unless S('.view.natural')? then move  1
	mc.on 'swiperight',    -> unless S('.view.natural')? then move -1
	[oldX, oldY] = [0, 0]
	mc.on 'pan',   (event) ->
		if event.pointerType is 'touch'
			S('.view:last-child').scrollBy event.velocityX*20, event.velocityY*20
		else
			S('.view:last-child').scrollBy (event.deltaX - oldX)*-1, (event.deltaY - oldY)*-1
			[oldX, oldY] = if event.isFinal then [0, 0] else [event.deltaX, event.deltaY]
		event.preventDefault()
	mc.on 'tap',   (event) ->
		if event.pointerType is 'mouse' then move 1
		else window.showPanel '#sidebar'
		event.preventDefault()
	mc.on 'press', (event) -> if event.pointerType isnt 'mouse'
		window.zoom(0, event.x, event.y)
		event.preventDefault()

@getBlob = (url, callbacks) ->
	window.currentDownload?.abort()
	window.currentDownload = xhr = new XMLHttpRequest()
	try xhr.responseType = 'blob' catch
		try xhr.responseType = 'arraybuffer' catch
			xhr.overrideMimeType 'text\/plain; charset=x-user-defined'
	xhr.open 'GET', url, true
	xhr.onerror = (error) ->
		@responseURL = @responseURL or url
		callbacks.error?.call @, error
	xhr.onprogress = (event) -> callbacks.progress? 'Downloading', if event.lengthComputable then event.loaded / event.total
	xhr.onload = -> if callbacks.load?
		res =
			response: switch getType @response
				when '[object Blob]'        then @response
				when '[object ArrayBuffer]' then new Blob [@response], type: @getResponseHeader 'Content-Type'
				when '[object String]'
					blob = new WebKitBlobBuilder()
					blob.append @response
					blob.getBlob @getResponseHeader 'Content-Type'
			responseURL: @responseURL or url
			status: @status or 200
		callbacks.load.call res
	xhr.send()

@download = (file, callbacks={}) ->
	if file.video?
		window.getBlob file.video, callbacks
		if callbacks.progress?
			S('#display-file').value = 'video'
			S('#display-file-default').classList.add 'hide'
	else
		hasTag = (tag) -> file.tags.indexOf(tag) isnt -1
		ext = file.original.split('.').pop()
		
		pixel_art = hasTag('pixel_art') or hasTag('oekaki')
		video     = ext is 'webm' or hasTag('webm')
		gif       = ext is 'gif'  or hasTag('gif') or hasTag('animated_gif')
		animated  = hasTag('animated') or hasTag('animated_gif')
		window.log "Downloading file", false,
			filename: file.original
			extension: ext
			pixel_art: pixel_art
			video: video
			gif: gif
			animated: animated
		if not (pixel_art or video) and gif and animated
			window.currentDownload?.abort()
			window.currentDownload = xhr = new XMLHttpRequest()
			xhr.open 'GET', "https://upload.gfycat.com/transcode?fetchUrl=#{encodeURI file.original}"
			xhr.setRequestHeader 'Cache-Control', 'no-cache'
			xhr.onerror = -> window.getBlob file[localStorage.getItem('default_file_source')] or file['original'], callbacks
			xhr.onload  = ->
				try if (res = JSON.parse @response) then file.video = switch
					when res.webmUrl? then res.webmUrl.replace('http://', 'https://')
					when res.gfyName? then "https://zippy.gfycat.com/#{res.gfyName}.webm"
					else undefined
				catch error then console.log "Failed to convert gif to webm:", error
				S('#display-file option[value="video"]').classList.remove 'close'
				S('#display-file').value = if file.video?
					S('#display-file-default').classList.add 'hide'
					'video'
				else if file[localStorage.getItem('default_file_source')]?
					S('#display-file-default').classList.add 'hide'
					localStorage.getItem('default_file_source')
				else 'original'
				window.getBlob file[S('#display-file').value], callbacks
			xhr.send()
			callbacks.progress? 'Converting to video'
		else
			url = if file[localStorage.getItem('default_file_source')]? then localStorage.getItem('default_file_source') else 'original'
			window.getBlob file[url], callbacks
			if callbacks.progress?
				S('#display-file').value = url
				if url is localStorage.getItem('default_file_source') then S('#display-file-default').classList.add 'hide'

@move = (amount) ->
	index += amount
	# check if out of range
	if     (index < 0)                       then index += window.results.length # go to end
	else if(index > window.results.length-1) then index -= window.results.length # go to start
	window.updateResultsCounter()
	window.currentFile = window.files[window.results[window.index]]
	window.updateThumbs()
	window.currentThumb?.scrollIntoView false
	S('#loading-file-percent').innerHTML = 'Connecting...'
	window.download window.currentFile,
		error: (error) -> window.display error
		progress: (text, done) ->
			S('#loading-file-percent').innerHTML = if done?
				if done < 1 then "#{text} (#{Math.floor done*100}%)" else 'Wait for it...'
			else text
		load: ->
			preload = if amount is 0 then window.index + 1 else window.index + Math.sign amount
			if      (preload < 0)                       then preload = window.results.length - 1 # preload end
			else if (preload > window.results.length-1) then preload = 0                         # preload start
			window.download window.files[window.results[preload]]
			window.display.call @
	window.loadingFile = true
	clearTimeout window.loadTimer
	window.loadTimer = after 0.2, -> if window.loadingFile then show '#loading-file', 'loadingFile', 'timeout'
	edge = Math.min window.results.length / 2, 50
	if window.results.length - index+1 < edge then search()

@aspect = ->
	windowAspect = window.innerHeight / window.innerWidth
	view = S '.view:last-child'
	if view? then content = S view, 'img, video'
	if content?
		# even though .naturalWidth/Height sounds format agnostic, they couldn't use it for video as well ¬_¬
		window.currentFile.aspect = window.currentFile.height / window.currentFile.width
		window.log "aspect", false,
			window:
				innerHeight: window.innerHeight
				innerWidth:  window.innerWidth
			view:
				clientHeight: view.clientHeight
				clientWidth:  view.clientWidth
				scrollWidth:  view.scrollWidth
				scrollHeight: view.scrollHeight
			content:
				clientHeight:  content.clientHeight
				clientWidth:   content.clientWidth
				naturalHeight: window.currentFile.height
				naturalWidth:  window.currentFile.width
				aspect:        window.currentFile.aspect
		content.classList.remove 'tall', 'wide', 'small'
		unless (view.classList.contains('natural') and content.clientHeight > view.clientHeight and content.clientWidth > view.clientWidth)
			content.classList.add if window.currentFile.aspect > windowAspect then 'tall' else 'wide'
		if window.currentFile.height < innerHeight and window.currentFile.width < innerWidth
			content.classList.add    'small'
		else
			content.classList.remove 'small'
		window.contentZoom = Math.floor content.clientHeight / window.currentFile.height * 100
		
		rect = content.getBoundingClientRect()
		notes = S view, '.notes'
		notes.style.top  = "#{if rect.top  < 0 then 0 else Math.round rect.top }px"
		notes.style.left = "#{if rect.left < 0 then 0 else Math.round rect.left}px"
		notes.style.height = "#{Math.round rect.height}px"
		notes.style.width  = "#{Math.round rect.width }px"
		for note in notes.contentDocument.querySelectorAll '.note'
			result = window.currentFile.results[note.getAttribute 'data-result']
			note.style.fontSize = "#{(content.clientHeight / result.height) * 100}%"
	else window.contentZoom = 100
	updateTitle()

@zoom = (amount, x, y) ->
	view = S '.view:last-child'
	view.classList.toggle 'natural'
	S('#sidebar').classList.add   'hide'
	S('#bottombar').classList.add 'hide'
	relScrX = x / view.clientWidth
	relScrY = y / view.clientHeight
	newx = Math.round (view.scrollWidth  * (x / view.clientWidth )) - (view.scrollWidth  - view.clientWidth )
	newy = Math.round (view.scrollHeight * (y / view.clientHeight)) - (view.scrollHeight - view.clientHeight)
	window.log 'zoom', false,
		event:
			x: x
			y: y
		scroll:
			width:  view.scrollWidth
			height: view.scrollHeight
		client:
			width:  view.clientWidth
			height: view.clientHeight
		zoom:
			x: newx
			y: newy
	view.scrollTo newx, newy
	window.aspect()

@addNotes = (site) ->
	view = S('.view:last-child')
	body = make 'tbody'
	result = window.currentFile.results[site]
	for note in result.notes
		[visible, checked] = if note.is_active and note.height > 0 and note.width > 0 then ['', true] else ['hide', false]
		element = make 'div', class: "note #{visible}", note.body, 'data-result': site, style: """
			top:    #{note.y      / result.height * 100}%;
			left:   #{note.x      / result.width  * 100}%;
			height: #{note.height / result.height * 100}%;
			width:  #{note.width  / result.width  * 100}%;
			"""
		S(view, '.notes').contentDocument.body.appendChild element
		do (body, element) ->
			body.appendChild make 'tr',
				make 'td', make 'input', type: 'checkbox', class: 'note-display', checked: checked, events: click: ->
					if @checked then element.classList.remove 'hide' else element.classList.add 'hide'
				make 'td', class: 'note-body', innerText: element.textContent
	S('#results-notes').replaceChild body, S '#results-notes tbody'
	if S('#display-notes').disabled
		window.viewOption '.view:last-child .notes', '#display-notes', 'hide', 'remove'
		S('#display-notes').disabled = false
	window.aspect()

@display = (error) ->
	window.loadingFile = false
	oldviews = document.querySelectorAll '.view'
	if error? is false and @status is 200
		[type, subtype] = @response.type.split '/'
		content = switch type
			when 'image' then make 'img',   class: 'center content', events:
				error: (error) -> if @src.slice(0, 5) is 'blob:' then @src = window.currentFile.url
				load: ->
					window.currentFile.height = @naturalHeight
					window.currentFile.width  = @naturalWidth
					window.aspect()
					view.classList.remove 'hide'
					clearTimeout window.loadTimer
					window.show '#loading-file', 'loadingFile'
			when 'video' then make 'video', class: 'center content', autoplay: true, loop: true, controls: false, events:
				error: -> if @error.code is @error.MEDIA_ERR_SRC_NOT_SUPPORTED and @src.slice(0, 5) is 'blob:'
					# fix issues with android's (version =< 4.4) broken html5 video support
					@loop = false # to fix looping ourselves we need to disable it otherwise 'ended' is never called
					@src = window.currentFile.original
					@play()
				ended: ->
					@currentTime = 0
					@play()
				loadedmetadata: ->
					window.currentFile.height = @videoHeight
					window.currentFile.width  = @videoWidth
					window.aspect()
				canplaythrough: ->
					@removeEventListener 'canplaythrough', arguments.callee
					view.classList.remove 'hide'
					clearTimeout window.loadTimer
					window.show '#loading-file', 'loadingFile'
			else
				nextFrame -> view.classList.remove 'hide' ; window.show '#loading-file', 'loadingFile'
				make 'div', class: 'center', "#{@response.type}<br>I don't know how to display this file 😕<br>You can try saving and opening this file in another application to view it"
		content.src = S('#save_file').href = S('link[rel="shortcut icon"]').href = window.URL.createObjectURL @response
		S('#save_file').download = @responseURL.slice @responseURL.lastIndexOf('/') + 1
		if window.currentFile?
			if window.currentFile.tags.indexOf('pixel_art') + window.currentFile.tags.indexOf('oekaki') > -1 then content.classList.add 'pixel_art'
			window.currentFile.blob = @response
	else
		content = if error? then switch error
			 when 'no sites'   then make 'div', class: 'center', "You don't have any sites enabled 😛"
			 when 'no results' then make 'div', class: 'center', "Sorry, no results were found 😞"
		else make 'div', class: 'center', "#{error or @status}<br>I couldn't download the file 😞", make 'button', 'Try again', events:
			click: (event) -> move 0 ; event.preventDefault()
		delete window.currentFile
		S('link[rel="shortcut icon"]').href = S('#save_file').download = ''
		nextFrame -> view.classList.remove 'hide' ; window.show '#loading-file', 'loadingFile'
	
	view = make 'div', class: 'view center hide fade-in', content, events:
		transitionend: -> for oldview in oldviews when oldview?
			window.URL.revokeObjectURL oldview.children[0]?.src
			try document.body.removeChild oldview catch error
	notes = make 'iframe', class: 'notes hide fade-in-out', sandbox: '', events: touchstart: (event) -> console.log 'touch iframe'
	view.appendChild notes
	
	clearTimeout window.loadTimer
	document.body.appendChild view
	notes.contentDocument.head.appendChild make 'style', """
		html, body { overflow: hidden } /* Fixes white borders on old android */
		:link
			{
			color: inherit;
			text-decoration: none;
			}
		.hide { display: none }
		.note
			{
			font-family: sans-serif;
			white-space: pre-line;
			position: absolute;
			text-shadow:
				 1px  1px 0.1em white,
				 1px  1px 0.2em white,
				-1px  1px 0.1em white,
				-1px  1px 0.2em white,
				 1px -1px 0.1em white,
				 1px -1px 0.2em white,
				-1px -1px 0.1em white,
				-1px -1px 0.2em white,
				 0    0   0.5em black,
				 0    0   0.5em black;
			}
		.note *
			{
			background: none !important;
			border:     none !important;
			}
		tn
			{
			color: hsl(0,0%,25%);
			font-size: 75%;
			}
		#{("font[size=\"+#{i}\"] { font-size: #{100+i*25}% }" for i in [1..7]).join('\n')}
	"""
	window.updateInfo()
