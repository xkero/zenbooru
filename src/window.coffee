action window, 'load', ->
	action window, 'resize', -> aspect() ; window.updateThumbs()
	
	action '#search',         'change', -> search             @value, true ; @blur()
	action '#highest_rating', 'change', -> search S('#search').value, true ; @blur()
	
	action '#go_to_first', 'click', -> move window.index = 0
	action '#go_previous', 'click', -> move -1
	action '#go_next',     'click', -> move  1
	
	action '#display-file', 'change', ->
		if @value isnt localStorage.getItem('default_file_source')
			window.show '#display-file-default'
		else S('#display-file-default').classList.add 'hide'
		S('#loading-file-percent').innerHTML = 'Connecting...'
		window.getBlob window.currentFile[@value],
			error: (error) -> window.display error
			progress: (text, done) ->
				S('#loading-file-percent').innerHTML = if done?
					if done < 1 then "#{text} (#{Math.floor done*100}%)" else 'Wait for it...'
				else text
			load: window.display
		window.loadingFile = true
		clearTimeout window.loadTimer
		window.loadTimer = after 0.2, -> if window.loadingFile then window.show '#loading-file', 'loadingFile', 'timeout'
	action '#display-file-default', 'click', ->
		localStorage.setItem 'default_file_source', S('#display-file').value
		@classList.add 'hide'
	action '#display-background', 'change', -> S('.view:last-child :first-child').style.backgroundColor = @value
	
	action '#display-scaling', 'click', -> window.viewOption '.view:last-child .content', '#display-scaling', 'pixel_art', 'toggle'
	action '#display-invert',  'click', -> window.viewOption '.view:last-child .content', '#display-invert',  'invert',    'toggle'
	action '#display-notes',   'click', -> window.viewOption '.view:last-child .notes',   '#display-notes',   'hide',      'toggle'
	
	action document,       'click', (event) -> unless S('input:focus, :link:hover')? then event.preventDefault()
	action document, 'contextmenu', (event) -> unless S('input:focus')? then event.preventDefault()
	action document,   'mousemove', (event) -> if not S('.frame:hover')? and event.buttons is 0
		if event.y > window.innerHeight - S('#bottombar').clientHeight then window.showPanel '#bottombar', true
		else
			window.showPanel '#bottombar', false
			window.showPanel '#sidebar', switch localStorage['interface_side']
				when 'left'  then event.x <                     (S('#sidebar').clientWidth / 2)
				when 'right' then event.x > window.innerWidth - (S('#sidebar').clientWidth / 2)
	
	action '#bottombar',     'scroll', window.updateThumbs
	action '#bottombar', 'mousewheel', (event) ->
		sign = if (event.wheelDelta or event.detail) > 0 then -1 else 1
		@scrollLeft += window.currentThumb.clientWidth*sign
		event.preventDefault()
	
	action '.fade-in-out, .fade-out', 'transitionend', -> if @classList.contains('hide') then @classList.add 'close', 'hide'
	action 'label[for]', 'click', -> S('#'+@htmlFor).dispatchEvent new MouseEvent 'mousedown' # click to mousedown is needed for this to work :/

@viewOption = (content, button, option, state) ->
	content = S content
	button  = S button
	set = if content?
		if state? then content.classList[state] option
		content.classList.contains option
	else false
	button.innerHTML = button.getAttribute "data-text-#{ set}"
	button.title     = button.getAttribute "data-title-#{set}"

@showPanel = (query, state, force) ->
	unless (panel = S query) then return
	allow = force or query is '#settings' or S('#settings.hide')?
	state = switch state
		when undefined then 'toggle'
		when true      then 'remove'
		when false     then 'add'
	if allow
		panel.classList[state] 'hide'
		unless panel.classList.contains 'hide'
			panels = ['#settings', '#sidebar', '#bottombar']
			if query is '#bottombar'
				if window.results.length > 0
					window.updateThumbs()
					panels.splice panels.indexOf(query), 1
			else panels.splice panels.indexOf(query), 1
			for panel in panels then S(panel).classList.add 'hide'
		if S('#sidebar.hide')? then S('#search').blur()

@updateTitle = ->
	document.title = if window.currentFile?
		counter = thousands(window.index+1) + '/' + thousands window.results.length
		more = ((for site of window.moreResults when window.moreResults[site] then '+')[0] or '')
		"#{counter+more} #{window.currentFile.checksum or 'Loading...'} [#{window.contentZoom or 100}%]"
	else "No results..."

@updateInfo = ->
	unless window.currentFile?
		for id in ['#results-counter', '#save_file', '#tags', '#results-site', '#results-source', '#results-uploaded', '#display-options']
			S(id).classList.add 'hide'
		return
	window.show '#results-counter, #save_file, #display-options', true
	
	window.viewOption '.view:last-child .content', '#display-scaling', 'pixel_art'
	window.viewOption '.view:last-child .content', '#display-invert',  'invert'
	window.viewOption '.view:last-child .notes',   '#display-notes',   'hide'
	S('#display-notes').disabled = true
	
	for url in ['original', 'sample', 'video'] then S("#display-file option[value=#{url}]")["#{if window.currentFile[url]? then 'remove' else 'set'}Attribute"] 'disabled', true
	
	has_notes = false
	for site, result of window.currentFile.results when result.notes?
		has_notes = true
		S('#results-notes tbody').innerHTML = 'Loading...'
		switch getType result.notes
			when '[object String]'
				xhr = new XMLHttpRequest()
				xhr.open 'GET', result.notes
				xhr.error  = -> window.show '#results-notes', false
				xhr.onload = ->
					result.notes = window.apis[window.sites[site].type].notes @response
					window.addNotes site
				xhr.send()
			when '[object Array]' then window.addNotes site
	window.show '#results-notes', has_notes
	
	tagsBody = make 'tbody'
	for tag in (if window.currentFile.rating? then window.currentFile.tags.concat [ "rating:#{window.currentFile.rating}" ] else window.currentFile.tags) then do (tag) ->
		[state, sign, title, callback] = if tag in window.getTags S('#search').value
			['included', '✕', 'Remove tag from current search', -> window.search window.removeTag tag]
		else ['',        '+', 'Add tag to current search',      -> window.search "#{S('#search').value} #{tag}"]
		type = '<span class="emoji">' + (switch window.tags[tag].type
			when 'artist'    then '✍'
			when 'character' then '⚉'
			when 'copyright' then '©'
			else                  '') + '</span> '
		tagsBody.appendChild make 'tr',
			make 'td', class: 'tag-exclude',     make 'button', '-',                              title: 'Exclude tag from current search', events:
				click: -> window.search window.addTag "-#{tag}", window.removeTag tag
			make 'td', class: 'tag-name '+state, make 'button', type+tag.replace(/_/g, '<wbr>_'), title: 'Start new search with this tag',  events:
				click: -> search tag
			make 'td', class: 'tag-include',     make 'button', sign,                             title: title,                             events:
				click: callback
			make 'td', class: 'tag-total', thousands window.tags[tag].total
	S('#tags').replaceChild tagsBody, S '#tags tbody'
	window.show '#tags'
	
	
	props =
		source:  'source'
		created: 'uploaded'
		page:    'site'
	for prop of props
		table = S "#results-#{props[prop]}"
		props[prop] = {}
		if (for site, result of window.currentFile.results when result[prop]? and result[prop].toString().replace(window.EXTRA_WHITESPACE, '') isnt ''
			data = result[prop].toString().replace(window.PIXIV_LINK, '//www.pixiv.net/member_illust.php?mode=medium&illust_id=$2')
			value = props[prop][data]
			if value? then value += ", #{site}"
			else           value  =      site
			props[prop][data] = value).length > 0
			tbody = make 'tbody'
			values = Object.keys props[prop]
			switch values.length
				when 0 then continue
				when 1 then tbody.appendChild make 'tr', make 'td',
					if values[0].match(window.URI_SCHEME)? then make 'a', target: '_blank', href: values[0], decodeURI(values[0]) else values[0]
				else for value in values
					tbody.appendChild make 'tr', make 'th', props[prop][value]
					tbody.appendChild make 'tr', make 'td',
						if value.match(window.URI_SCHEME)? then make 'a', target: '_blank', href: value, decodeURI(value)
						else value
			table.replaceChild tbody, S table, 'tbody'
			window.show table
		else table.classList.add 'hide'

@addThumbs = (reset=false) ->
	if reset
		S('#thumbs').innerHTML = ''
		S('#thumbs').scrollTo 0, 0
	diff = window.results.length - document.querySelectorAll('#thumbs .thumb').length
	if diff > 0
		thumbs = document.createDocumentFragment()
		diff++
		while diff -= 1 then do (i=window.results.length - diff) ->
			file = window.files[window.results[i]]
			thumbs.appendChild make 'button', class: 'thumb', title: file.tags.join(' '),'data-src': file.thumb, '<img>', events: click: ->
				window.index = i
				window.updateThumbs()
				move 0
		S('#thumbs').appendChild thumbs
	window.moreLock = false

@updateThumbs = ->
	previousThumb       = window.currentThumb
	window.currentThumb = S("#thumbs .thumb:nth-child(#{window.index+1})")
	if window.currentThumb isnt previousThumb
		previousThumb?.classList.remove 'included'
		window.currentThumb?.classList.add 'included'
	if S('#bottombar.hide')? then return
	start = Math.floor S("#bottombar").scrollLeft/window.currentThumb.clientWidth
	end   = Math.ceil start+(S('#thumbs').clientWidth/window.currentThumb.clientWidth)
	for i in [start..end]
		thumb = S('#thumbs').children[i]
		thumb?.firstChild.src = thumb.getAttribute('data-src')
	if window.moreLock is false and end is S('#thumbs').children.length-1
		window.moreLock = true
		search()

@updateResultsCounter = ->
	S('#current_index').innerHTML = thousands (window.index+1 or 0).toString()
	S('#last_index').innerHTML    = thousands window.results.length.toString() + ((for site of window.moreResults when window.moreResults[site] then '+')[0] or '')

@addTag    = (tag, string=S('#search').value) -> if string.slice(-1).match(/\s|^$/) then "#{string}#{tag}" else "#{string} #{tag}"
@removeTag = (tag, string=S('#search').value) ->
	tag = window.regexEscape tag
	string.replace new RegExp("(?=^|\\s)-?#{tag}\\s?|\\s?-?#{tag}(?=\\s|$)", 'gi'), ''

@search = (tags=S('#search').value, reset) ->
	searching = (site for site of window.sites when window.sites[site].enabled)
	window.reset        = reset or tags isnt window.previousSearch
	S('#search').value  = window.previousSearch = tags
	window.includeTags  = (tag for tag in window.getTags(tags) when tag.match(/^-|^rating:\w+$/) is null)
	window.resultsTotal = searching.length
	window.resultsDone  = 0
	window.messageDelay = 0
	window.anyResults   = false
	
	excludedTags = make 'tbody'
	for tag in window.getTags(tags) when tag[0] is '-' then do (tag, tagName=tag.slice 1) ->
		excludedTags.appendChild make 'tr',
			make 'td', class: 'tag-exclude',       make 'button', '✕',                             title: 'Stop excluding tag from current search',
				events: click: -> window.search window.removeTag tag
			make 'td', class: 'tag-name excluded', make 'button', tagName.replace(/_/g, '<wbr>_'), title: 'Start new search with this tag',
				events: click: -> window.search tagName
			make 'td', class: 'tag-include',       make 'button', '+',                             title: 'Add tag to current search',
				events: click: -> window.search S('#search').value.replace tag, tagName
			make 'td', class: 'tag-total', thousands window.tags[tagName]?.total or 0
	if excludedTags.childElementCount > 0
		window.requestAnimationFrame ->
			S('#excluded_tags').replaceChild excludedTags, S('#excluded_tags tbody')
			S('#excluded_tags').classList.remove 'close'
			window.requestAnimationFrame -> S('#excluded_tags').classList.remove 'hide'
	else S('#excluded_tags').classList.add 'hide'
	
	S('#messages').innerHTML = ''
	S('#loading-results').style.width = '0'
	S('#loading-results').classList.remove 'error', 'info', 'warn'
	if window.reset
		window.resultsPage = 0
		window.moreResults = {}
		window.results     = []
		window.loadingFile = true
		window.show '#loading-file', 'loadingFile', 'reset'
	if searching.length > 0
		tags += switch S('#highest_rating').value
			when 'safe'         then ' rating:safe'
			when 'questionable' then ' -rating:explicit'
			else                     ''
		page = window.resultsPage
		for site in searching then do (site) ->
			if window.moreResults[site] ?= true then window.getResults window.apis[window.sites[site].type], site, tags, page, 100, window.mergeResults
			else window.resultsTotal--
		window.resultsPage++
	else
		window.loadingFile = false
		window.show '#loading-file', 'loadingFile', 'no sites'
		window.showPanel '#settings', true
		window.display 'no sites'
